# Dijkstra's Algorithm 
Cassie Chin   
Email:      cassiechin9793@gmail.com   
Date:       April 26, 2014   

Input File format:
Picture Diagram of the sample input: http://students.cse.unt.edu/~clc0441/oldSite/csce3530/diagram.png   

This program takes a graph in the input.txt file and generates minimum distances using Dikstra's Algorithm.
The output is printed to the console. This program only supports one test at a time. An input.txt file is provided. 
The first line of the file must contain the number of total nodes followed by a space followed by the starting node. 
Each subsequent line has to include the node on one side of the edge, followed by a single space, followed by the
node on the other side of the edge followed by a return. A distance from node A to node B is assumed to be the
same distance from node B to node A. Thus in the input file, it is not necessary to include (A B distance) and 
(B A distance).   
   
Output format:   
The graph is print out the minimum distance for each node.  

### HOW TO USE THIS PROGRAM ###
The input file must be named "input.txt"   

There must be a single return at the end of the input file.   

STEP 1: Compile using "gcc main.c -o main"   
STEP 2: Run using "./main"