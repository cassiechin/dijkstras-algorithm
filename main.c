/*
 * Cassie Chin (cassiechin9793@gmail.com)
 *
 * Dijkstra's Algorithm
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>

#define PRINT_INPUT_EDGES_OFF // turn this on to see what your input was
#define NO_EDGE -2 // Used in the adjacency matrix
#define INFINITY INT_MAX // Used in the visited array

// This is used for the visited_flag array
// Everything is initially 0
// When a distance is initialized (after being infinity) the flag goes to 1
// When the distance is the lowest distance in the distance array, the flag goes to 2
#define PUSH 1
#define POP 2

int main () {
  FILE *f_in = fopen ("input.txt", "r");
  if (!f_in) return 0;

  int i,j;

  int start = 0;
  int num_of_nodes = -1; // this determines the dimensions of the adjacency matrix
  int **adjacency_matrix; //  the static distances are stored here, the axis are the two and from vertices and the value is the distance

  int *visited_flag; // indicates if a node has been visited or not, doesn't say anything about the order, 1 indicates visited, 2 indicates popped
  int target_index; // this is the index of the node whose neighbors are being evaluated
  int visited_counter = 0; // the nodes are stored in order, this counter is used to show what the next empty array slot is
  int *distance; // stores the smallest distances, the index is the vertex number and the value is the distance

  char *buffer = (char *) calloc (256, sizeof(char));
  while (fgets(buffer, 256, f_in)) {
    buffer[strlen(buffer)-1] = '\0'; // remove the new line character at the end

    // This variable is initialized to -1, so when this is set it should always be set to a positive number
    if (num_of_nodes == -1) {
      sscanf(buffer, "%d %d", &num_of_nodes, &start);
      adjacency_matrix = (int **) calloc (num_of_nodes + 2, sizeof(int *)); // calloc an array of integer arrays
      int i;
      for (i=0; i<num_of_nodes+2; i++) {
	adjacency_matrix[i] = (int *) calloc (num_of_nodes + 2, sizeof(int)); // calloc space in each of those arrays
      }

      // Set up the adjacency matrix by marking all the values as having no edge
      for (i=0; i<num_of_nodes+2; i++) {
	for (j=0; j<num_of_nodes+2; j++) {
	  adjacency_matrix[i][j] = NO_EDGE;
	}
      }

      // set all the distances to infinity
      distance = (int *) calloc (num_of_nodes + 2, sizeof(int));
      for (i=0; i<num_of_nodes+2; i++) 	distance[i] = INFINITY;

      // Set all the flags to 0
      visited_flag = (int *) calloc (num_of_nodes +2, sizeof(int));
    }
    else {
      int nodeA, nodeB, distance;
      sscanf(buffer, "%d %d %d", &nodeA, &nodeB, &distance);
      // This program assumes that the distance between nodeA - nodeB is the sames as the distance between nodeB - nodeA
      adjacency_matrix[nodeA][nodeB] = distance;
      adjacency_matrix[nodeB][nodeA] = distance;
    }
  }

  // Print adjacency matrix
#ifdef PRINT_INPUT_EDGES_ON
  printf("Input graph\n");
  for (i=0; i<=num_of_nodes; i++) {
    for (j=0; j<=num_of_nodes; j++) {
      if (adjacency_matrix[i][j] != NO_EDGE)
	printf("Edge (%d, %d) has a distance of %d\n", i,j, adjacency_matrix[i][j]);
    }
    printf("\n");
  }
#endif

  // File has been processed, so now start Dijkstra's Algorithm

  // mark number 1 as visited and set it's distance to 0
  visited_flag[start] = POP; // mark the first node as visited
  distance[start] = 0; // set the initial distance to 0
  target_index = start; // this is the pointer that "follows" the visited_counter pointer

  //  do {
  while (1) {
    for (i=0; i<=num_of_nodes; i++) {
      if (adjacency_matrix[target_index][i] == NO_EDGE) continue;
      
      // do something if its neighbors distance is infinity, or its neighbors distance is greater than the distance to the target plus the distance from the target to the neighbor
      if (distance[i] > distance[target_index] + adjacency_matrix[target_index][i]) {
	// Update the distance by adding the current distance to the target_index and the distance between the target index and i
	distance[i] = distance[target_index] + adjacency_matrix[target_index][i];

 	// Indicates that the neighbor has not already been visited
	if (visited_flag[i] == 0) {
	  visited_flag[i] = 1; // set the visited flag of the neighbor node to 1
	}
      }
    }
    int smallest = INFINITY;
    int smallest_index = 0;

    // move the target index to the smallest element in the visited array
    // Search through the distance array for the smallest element
    // However, only search if the element has not been "popped"already
    for (i=0; i<=num_of_nodes; i++) {
      if (visited_flag[i] == 1) { // This node has been visited but not popped
	if (distance[i] < smallest) { 
	  smallest_index = i;
	  smallest = distance[i];
	}
      }
    }

    //    printf("The smallest distance is %d\n", smallest);
    // Save the next smallest item in the array
    target_index = smallest_index;
    // Set the visited flag to 2 so this item won't get popped again
    visited_flag[target_index] = 2;

    if (smallest == INFINITY) break;
    // If there is nothing left in the dstance array then quit
  }
    //  } while (smallest != INFINITY);

  //  printf("Distance array\n"); 
  for (i=0; i<=num_of_nodes; i++) {
    if (distance[i] == 0 || distance[i] == INFINITY) continue;
    printf("The distance from %d to %d is %d.\n", start, i, distance[i]);
  }
  //  printf("\n");

  fclose (f_in);

  return 0;
}
